import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule} from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationComponent } from './components/navigation/navigation.component';
//formulario
import {FormsModule} from '@angular/forms'
import { ReactiveFormsModule } from '@angular/forms';
// firebase
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { AngularFireAuthModule } from '@angular/fire/auth';
//import { ToastrModule } from 'ngx-toastr'; //npm i ngx-toastr

import { GameFormComponent } from './components/mysql/game-form/game-form.component';
import { GameListComponent } from './components/mysql/game-list/game-list.component';
import { GamesService } from './services/games.service';
import { GamesTableComponent } from './components/mysql/games-table/games-table.component';
import {AddDataModule} from './components/firebase/add-data/add-data.module';
import {GetDataModule} from './components/firebase/get-data/get-data.module';
import {AdminComponent } from './components/firebase/admin/admin.component';
import { AddformComponent } from './components/firebase/admin/form/addform/addform.component';
import { UpdateformComponent } from './components/firebase/admin/form/updateform/updateform.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    GameFormComponent,
    GameListComponent,
    GamesTableComponent,
    AdminComponent,
    AddformComponent,
    UpdateformComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    AddDataModule,
    //ToastrModule.forRoot(),
    GetDataModule,
    ReactiveFormsModule,
    FormsModule,
    AngularFirestoreModule,
    AngularFireStorageModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule,
  ],
  providers: [
    GamesService,
   
  ],
  bootstrap: [AppComponent],
  
})
export class AppModule { }
