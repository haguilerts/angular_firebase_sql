import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GamesService } from 'src/app/services/games.service';

@Component({
  selector: 'app-game-list',
  templateUrl: './game-list.component.html',
  styleUrls: ['./game-list.component.scss']
})
export class GameListComponent implements OnInit {
  game:any=[]
  constructor(private gamesService:GamesService, private router:Router) { }

  ngOnInit(): void {
   this.stardGame()
  }
  stardGame(){
    this.gamesService.getAll_anime().subscribe(
      res=>{
       //console.log(res.title)
        this.game=res
      },
      err=>console.log(err)
    )
  }
  deleteGame(id:any){
    //console.log( id)
    this.gamesService.delete_Anime(id).subscribe(
      res=>{
       // console.log(res)
        this.stardGame()        
      },
      err=>console.log(err)
    )
  }
  updateGame(id:any){
    this.router.navigate([`mysql/edit/${id}`])
  }


}
