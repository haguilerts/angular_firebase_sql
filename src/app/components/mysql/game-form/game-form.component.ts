/* import { trimTrailingNulls } from '@angular/compiler/src/render3/view/util'; */
import { Component, HostBinding, OnInit, SimpleChange } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import { Game } from 'src/app/models/Game';
import { GamesService } from 'src/app/services/games.service';

@Component({
  selector: 'app-game-form',
  templateUrl: './game-form.component.html',
  styleUrls: ['./game-form.component.scss']
})
export class GameFormComponent implements OnInit {
  imgDefecto:string=`https://s5.postimg.cc/537jajaxj/default.png`
  @HostBinding('class') clases='row'
  game:Game={
    id:0,
    title:'',
    descripcion:'',
    imagen:'',
    CREATED_at:new Date()
  }
  urlImg:boolean=true
  edit:boolean=false;

  constructor(private gamesService :GamesService, private router:Router, 
    private paramURL:ActivatedRoute) {
    
  }

  ngOnInit(): void {
    const params = this.paramURL.snapshot.params;
    if (params.id) {
      this.gamesService.getOne_Anime(params.id)
        .subscribe(
          res => {
            this.game= res[0];
            this.edit = true;
            
          },
          err => console.log(err)
        )
      //console.log("--Game: ", this.game)
    }
  }
  /* 
  this.gamesService.getGame(this.paramURL.snapshot.params.id)
      .subscribe(
        res=>{
          console.log("res: ",res)
          this.game = res;
          this.edit=true
          console.log("Game: ", this.game)
        },
        err=>console.error(err)
      )
  */
  //------------------------------------------------
  updateGame(){
    delete this.game.CREATED_at;
    const urlDrive='https://drive.google.com/uc?export=view&id='
    if(this.game.imagen?.trim().startsWith('https://drive')){
      this.game.imagen=this.game.imagen?.replace('https://drive.google.com/file/d/',urlDrive).replace('/view?usp=sharing','')
    }
    this.gamesService.update_Anime(this.paramURL.snapshot.params.id, this.game)
      .subscribe(
        res => { 
         // console.log(res);
          this.router.navigate(['/']);
          //console.log('Se guardo correctamente... ')
        },
        err => console.error(err)
      )
  }
  imgDrive(){
    const urlDrive='https://drive.google.com/uc?export=view&id='
    if(this.game.imagen?.trim().startsWith('https://drive')){
      this.game.imagen=this.game.imagen?.replace('https://drive.google.com/file/d/',urlDrive).replace('/view?usp=sharing','')
    }
    //console.log('this.game.imagen',this.game.imagen)
  }
  saveGame(){
    delete this.game.id;
    delete this.game.CREATED_at;
    //console.log(this.game)
    //const urlDrive='https://drive.google.com/uc?export=view&id='
    //console.log(this.game.imagen?.substring(8,13))
    //if(this.game.imagen?.trim().startsWith('https://drive')){
    //  this.game.imagen=this.game.imagen?.replace('https://drive.google.com/file/d/',urlDrive).replace('/view?usp=sharing','')
    //}
    this.imgDrive()
    //console.log(this.game)
    this.gamesService.post_Anime(this.game)
      .subscribe(
        res=>{
         // console.log(res)
          this.router.navigate(['/'])
        },
        err=>console.error(err)
      )
  }

}
