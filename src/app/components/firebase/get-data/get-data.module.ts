import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

//import { GetDataRoutingModule } from './get-data-routing.module';
import { GetDataComponent } from './get-data.component';


@NgModule({
  declarations: [
    GetDataComponent
  ],
  imports: [
    CommonModule,
   // GetDataRoutingModule
  ]
})
export class GetDataModule { }
