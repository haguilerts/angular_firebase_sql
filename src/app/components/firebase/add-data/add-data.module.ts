import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddDataComponent } from './add-data.component';
//form
import {FormsModule} from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
@NgModule({
  declarations: [
    AddDataComponent
  ],
  imports: [
    CommonModule,   
    FormsModule,
    ReactiveFormsModule
  ]
})
export class AddDataModule { }
