import { Component, HostBinding, OnInit, SimpleChange } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute,Router } from '@angular/router';
import { PostService } from 'src/app/services/post.service';
import {PostI} from '../../../models/post.interface'
@Component({
  selector: 'app-add-data',
  templateUrl: './add-data.component.html',
  styleUrls: ['./add-data.component.scss']
})
export class AddDataComponent implements OnInit {
  imgDefecto:string=`https://s5.postimg.cc/537jajaxj/default.png`
  @HostBinding('class') clases='row'

  urlImg:boolean=true
  edit:boolean=false;
  img:any;
  public form = new FormGroup({
    title: new FormControl('', Validators.required),
    content: new FormControl('', Validators.required),
    imagen: new FormControl('', Validators.required),
    color: new FormControl('', Validators.required),
  });
  constructor(private service:PostService) {}

  ngOnInit(): void {
   
  }
  
  //------------------------------------------------
  updateGame(){
    
  }
  
  saveGame(){
    
  }
  //-------------------------------------------------
  addImage(e:any):void{
    //console.log(e.target.files[0])
    this.img=e.target.files[0]
    //console.log(this.img)
  }
  addNewPost(data:PostI){
    if(this.form.validator){
      console.log('campos validos')
    }
    console.log(data)
    this.service.addPost(data,this.img)

  }
}
