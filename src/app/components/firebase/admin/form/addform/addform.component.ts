import { Component, Input, OnInit, SimpleChange } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { PostI } from 'src/app/models/post.interface';
import { PostService } from 'src/app/services/post.service';


@Component({
  selector: 'app-addform',
  templateUrl: './addform.component.html',
  styleUrls: ['./addform.component.scss']
})
export class AddformComponent implements OnInit {
  @Input('data') datos:PostI;
  //datos:any;
  inTitulo:string=''
  formAnime:FormGroup;
  private img:any;
  private imgOriginal:any;
  mytitulo:string=''
  imgDefecto:string='https://s5.postimg.cc/537jajaxj/default.png'
  constructor(private postSvc: PostService) { 
    this.datos={
      title: '',
      content: '',
      imagen: '',
      id: '',
      color: '',
      
    }
    this.formAnime=new FormGroup({
      id: new FormControl('', Validators.required),
      title:new FormControl(' ',[Validators.required]),
      content:new FormControl('',[Validators.required]),
      color:new FormControl('',[Validators.required]),
      imgUrl:new FormControl('',[Validators.required]),
    })
  }

  ngOnInit(): void {
    console.log('estas oninit')
    //this.datos=this.inputDatas.dato
  }
  ngOnChanges(changes:SimpleChange){
    console.log('ngOnChanges ---- star ------------')
    console.log(changes)
    if(this.datos.id){
      console.log('this.datos.id: ',this.datos.id)
      this.initValuesForm();
    }
    //console.log(`# LIstaaa : ${changes.currentValue}/, : ${changes.previousValue}`)
    
    console.log('ngOnChanges ----- end ------------')
  }
  handleImg(event:any){
    console.log('img: ',event.target.files[0].mozFullPath)
    this.img=event.target.files[0];
    this.datos.imagen=event.target.files[0].name;
  }
  onSubmit(valor: PostI){
    if(this.datos.id){
      //actualizando
      //this.initValuesForm();
      this.update(valor)
    }else{
      this.addNewPost(valor)
    }

    console.log('onSubmit: ',valor)
  }
  addData(){

  }
 /*  addImage(e:any):void{
    //console.log(e.target.files[0])
    this.img=e.target.files[0]
    //console.log(this.img)
  } */
  addNewPost(data:PostI){
    if(this.formAnime.validator){
      console.log('campos validos')
    }
    console.log(data)
    this.postSvc.addPost(data,this.img)

  }

  update(upData: PostI){
    console.log('img update: ',this.img)
    console.log('img Original: ',this.imgOriginal)
    if(this.img==this.imgOriginal){
      //si no cambio la img
      //this.datos.imagen
      this.postSvc.editPostById(upData);
      console.log('actualizando...')
    }else{
      //si cambio
      this.postSvc.editPostById(upData, this.img);
      console.log('insertando...') 
    }
  }
 
  private initValuesForm(): void {
    console.log('start')
    this.formAnime.patchValue({
      id: this.datos.id,
      title: this.datos.title,
      content: this.datos.content,
      color: this.datos.color,
      //imgUrl:this.datos.imagen
    });
    console.log('end')
    this.img = this.datos.imagen;
    this.imgOriginal = this.datos.imagen;
    console.log('123 end')

  }

}
