import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PostI } from 'src/app/models/post.interface';
import { PostService } from 'src/app/services/post.service';
import Swal from 'sweetalert2'; //npm install sweetalert2 (es un alerta)

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  oneData:any;
  arryAnime:any=[]
  constructor(private AnimeService:PostService, private router:Router) { 
    this.oneData=''
  }

  ngOnInit(): void {
    this.stardGame()
  }
  stardGame(){
    this.AnimeService.getAllPosts().subscribe(
      res=>{
        console.log('res: ',res)
        this.arryAnime=res        
      },
      err=>console.log(err)
    )
  }
  addData(option:string){
    console.log('estas en añadir!!')
    let dato={
      title:'',
      content:'',
      color:'',
      imagen:'https://s5.postimg.cc/537jajaxj/default.png'
    }
    this.oneData=dato

  }
  updateData(dato:PostI){
    console.log('estas en actualizar!!')
    this.oneData=dato
    console.log('Edit post',dato);
  }
  deleteData(data:any){
    console.log(data)
    Swal.fire({
      title: 'Está seguro?',
      text: `No podrás revertir esto!`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, bórralo!'
    }).then(result => {
      if (result.value) {
        this.AnimeService.deletePostById(data).then(() => {
          Swal.fire('Eliminando!', 'Tu publicación ha sido eliminada', 'success');
        }).catch((error) => {
          Swal.fire('Error!', 'Se produjo un error al eliminar esta publicación.', 'error');
        });
      }
    });
  }

}
