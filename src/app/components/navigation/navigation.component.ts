import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {
  mostrarMenu:string[];
  on:boolean
  activo:any[];
  constructor( private ruta:Router) { 
    this.mostrarMenu=[]
    this.on=true
    this.activo=[null,null,null,null]
  }

  ngOnInit(): void {
    this.mostrarMenu=['']
    this.activo[0]='active'
  }
  onclickResposil(id:any){
    //menu_vuelta / 
    const menu=document.querySelector('.direc-menu')
    console.log(menu?.classList.contains('direc-menu_ida'))
    this.on=!this.on
    if(menu?.classList.item(2)==='direc-menu_ida'){
      this.mostrarMenu[0]='direc-menu_vuelta'
    }else{
      this.mostrarMenu[0]='direc-menu_ida'
    }

    console.log(this.mostrarMenu[0])
  }
  cambiarPage(page:string){
    console.log(page)
    this.ruta.navigate([page]);
    //this.cambiarLinck(page)
  }
  private clearLinck(){
    for (const i in this.activo) {
      this.activo[i]=null
    }
  }
  private cambiarLinck(linkPage:string){
    switch(linkPage){
      case 'mysql_get':
        this.clearLinck();
        this.activo[0]='active'
        break;
      case 'mysql/add':
        this.clearLinck();
        this.activo[0]='active'
        break;
      case 'mysql/admin':
        this.clearLinck();
        this.activo[0]='active'
        break;
     
      case 'firebase/addData':
        this.clearLinck();
        this.activo[1]='active'
        break;
      case 'firebase/getData':
        this.clearLinck();
        this.activo[1]='active'
        break;
      case 'firebase/admin':
        this.clearLinck();
        this.activo[1]='active'
      break;
    }
  }
  cartas(simbolo:any[]){
    let misCartas=[]
    for (let i = 0; i < simbolo.length; i++) {
      if(simbolo[i]!=8 || simbolo[i]!=9 ){
        misCartas.push(simbolo[i])
      }      
    }
    return misCartas;
  }


}
