export interface PostI {
    title: string;
    content: string;
    imagen?: any;
    id?: string;
    color?: string;
    time?:Date
    //fileRef?: string;
  }
export interface FileI {
  name?: string;
  imageFile?: File;
  size?: string;
  type?: string;
}
  