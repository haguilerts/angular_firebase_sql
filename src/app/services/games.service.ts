import { Injectable } from '@angular/core';
import {HttpClient  } from '@angular/common/http';
import {Game} from '../models/Game'
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class GamesService {
  Api_url='http://localhost:3200/api'
  constructor(private http:HttpClient) { }

  getAll_anime():Observable<Game>{
    return this.http.get(`${this.Api_url}/anime`)
  }

  getOne_Anime(id:string):Observable<Game[]>{
    return this.http.get<Game[]>(`${this.Api_url}/anime/${id}`)
  }
  delete_Anime(id:string):Observable<Game>{
    return this.http.delete(`${this.Api_url}/anime/${id}`)
  }
  update_Anime(id:string|number, update:Game):Observable<Game>{
    return this.http.put(`${this.Api_url}/anime/${id}`,update)
  }

  post_Anime(game:Game){
    //console.log('game server: ',game)
    return this.http.post(`${this.Api_url}/anime/`,game)
  }
  /* 
  getGame(id:string):Observable<Game>{
    return this.http.get(`${this.Api_url}/anime/${id}`)
  } */

}
