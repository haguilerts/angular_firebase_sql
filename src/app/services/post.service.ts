import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';
import { FileI, PostI } from '../models/post.interface';

@Injectable({
  providedIn: 'root'
})
export class PostService {
  private postsCollection: AngularFirestoreCollection<PostI>;
  private packageImg: any;
  private urlImg: Observable<string>;
  //---------   constructor --------------
  constructor( private angfire: AngularFirestore, private angFileStorage: AngularFireStorage) { 
    this.postsCollection = angfire.collection<PostI>('Anime');
    this.urlImg=new Observable()
  }
  //-------------------- GET ---------------------
  public getAllPosts(): Observable<PostI[]>  {
    return this.postsCollection.snapshotChanges().pipe(
        map(actions =>
          actions.map(a => {
            const data = a.payload.doc.data() as PostI;
            const id = a.payload.doc.id;
            console.log('dataAllFirebase: ',id,data)
            return { id, ...data };
          })
        )
       
      );
  }
  public getAll():Observable<any> {
    console.log('GetAll(): ',this.angfire.collection('Animes').snapshotChanges())
    console.log(this.postsCollection)
    return this.angfire.collection('Anime',ref=> ref.orderBy('fechaCreacion','asc')).snapshotChanges();
  }
  //-------------------- GET one ---------------------
  public getOnePost(id: PostI){

  }
  //-------------------- Delete ---------------------
  public deletePostById(id: string) {
    return this.postsCollection.doc(id).delete();
  }
  //-------------------- update ---------------------
  public editPostById(post: PostI, Image?: FileI) {
    if (Image) {
      return this.PushImg(post, Image);
      console.log('cambio de img- actualizacion con Exitooo')
    }else{
      // cuando no se cambia la img
      return this.postsCollection.doc(post.id).update(post);
      console.log('NO cambio img - actualizacion con Exitooo ')
    } 
  }
  //-------------------- Push ---------------------
  public addPost(data: PostI, newImage: FileI) {
    this.PushImg(data, newImage);
  }
  // =================== private =========================
  private saveData(data:PostI){
    let objData={
      //id: Math.random().toString(36).substring(2),
      title: data.title,
      content:data.content,
      imagen:this.urlImg,
      packageImage:this.packageImg,
      color:data.color,
      time:new Date()
    }
    if(data.id){
      //actualiza     
      return this.postsCollection.doc(data.id).update(objData);
    }else{
      //agrega      
      console.log('guardado con exito....')
      return this.postsCollection.add(objData);
    }
  }
  private PushImg(data:PostI, img:FileI) {
    this.packageImg=`Anime/${img.name}`;
    const fileRef=this.angFileStorage.ref(this.packageImg);
    const tacks = this.angFileStorage.upload(this.packageImg,img)
      tacks.snapshotChanges().pipe(
        finalize( ()=>{
          fileRef.getDownloadURL().subscribe( urlImage=>{
            this.urlImg=urlImage;// obtengo la direcion de la imagen guardad en el storage
            console.log('url img push: ',urlImage)   
            console.log('url data push: ',data)
            this.saveData(data)
          })
        })
      ).subscribe();
  }
}
