import { NgModule } from '@angular/core';
//import { enableDebugTools } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { AddDataComponent } from './components/firebase/add-data/add-data.component';
import { AdminComponent } from './components/firebase/admin/admin.component';
//import { AddformComponent } from './components/firebase/admin/form/addform/addform.component';
import { GetDataComponent } from './components/firebase/get-data/get-data.component';
import { GameFormComponent } from './components/mysql/game-form/game-form.component';
import { GameListComponent } from './components/mysql/game-list/game-list.component';
import { GamesTableComponent } from './components/mysql/games-table/games-table.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/' },
  { path: '', component: GetDataComponent },
 // { path: 'mysql_get', component:  GameFormComponent },
  { path: 'mysql/get', component: GameListComponent },
  { path: 'mysql/add', component: GameFormComponent },
  { path: 'mysql/edit/:id', component: GameFormComponent },
  { path: 'mysql/admin', component: GamesTableComponent },

  { path: 'firebase/getData',component: GetDataComponent},
  { path: 'firebase/addData', component: AddDataComponent },
  { path: 'firebase/edit/:id', component: GameFormComponent },
  { path: 'firebase/admin', component: AdminComponent },

  { path: '**', redirectTo:'/'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled', initialNavigation: 'enabled' })],

  exports: [RouterModule]
})
export class AppRoutingModule { }
