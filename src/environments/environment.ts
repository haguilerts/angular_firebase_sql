// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCLlQCRUVgI6e4b5MOn7Xt2cNc3tVtnMF0",
    authDomain: "angularproyect-308220.firebaseapp.com",
    projectId: "angularproyect-308220",
    storageBucket: "angularproyect-308220.appspot.com",
    messagingSenderId: "704542587055",
    appId: "1:704542587055:web:25dccb0cebbb14ef4afc5a",
    measurementId: "G-WMJLY8J92F"
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
